FROM python:3.7

COPY . /app
WORKDIR /app

RUN pip install --no-cache-dir cython \
    && python setup.py build_ext --inplace \
    && pip install . \
    && rm -r Dockerfile README.md pkg/

CMD ["python", "main.py"]

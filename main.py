import timeit

print(timeit.timeit("run()", setup="from pkg.test import run"))
print(timeit.timeit("run()", setup="from pkg.sub.test import run"))

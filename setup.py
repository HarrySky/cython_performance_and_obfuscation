import os
from setuptools import setup, Extension
from setuptools.command.build_py import build_py as build_py_orig
from Cython.Build import build_ext


class build_py(build_py_orig):
    def build_packages(self):
        pass


ext_modules = [
    Extension(
        name=f"{root_dir.replace('/', '.')}.{filename.replace('.py', '')}",
        sources=[f"{root_dir}/{filename}"]
    )
    for root_dir, _, files in os.walk("pkg")
    for filename in files
    if ".py" in filename and filename != "__init__.py" and "__pycache__" not in root_dir
]
for ext in ext_modules:
    ext.cython_directives = {'language_level': "3"}

setup(
    name='pkg',
    version="0.0.1",
    cmdclass={
        'build_ext': build_ext,
        'build_py': build_py
    },
    packages = ['pkg'],
    ext_modules=ext_modules,
    # Use MANIFEST.in for data files
    include_package_data=True,
)
